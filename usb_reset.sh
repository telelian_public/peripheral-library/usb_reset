#!/bin/sh
# io number : nano(64), xavier(268)
IO_NUM=$1
IO_PATH="/sys/class/gpio/gpio$IO_NUM"
if [ -e $IO_PATH ]; then
    echo "io $IO_NUM exist"
else
    echo "io $IO_NUM export"
    echo $IO_NUM > /sys/class/gpio/export
    sleep 0.5
fi

echo "out" > $IO_PATH/direction
echo 1 > $IO_PATH/value
sleep 0.5
echo 0 > $IO_PATH/value