chmod 744 usb_reset.sh
chmod 664 usb_reset-nano.service
sudo cp usb_reset.sh /usr/local/bin/
sudo cp usb_reset-nano.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable usb_reset-nano.service
